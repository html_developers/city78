$(document).ready(function() {

    var win_resize = function() {
      // $('.main_slider').css({height:  $('.main_slider').width()*41.5/100 + 'px'});
    };

    win_resize();

    $(window).resize(function() {
      win_resize();
    });

    $('.wrapper_header .menu ul li').mouseenter(function() {
      var classs = $(this).attr('class');
      $('.level2 ul').hide();
      $('.level2').show();
      $('.level2 .' + classs).show();
    });
    $('.wrapper_header .menu ul li').mouseleave(function() {
      $('.level2').hide();
    });

    $('.level2').mouseenter(function() {
      $('.level2').show();
    });

    $('.level2').mouseleave(function() {
      $('.level2 ul').hide();
      $('.level2').hide();
    });

    $('#slider2').tinycarousel();

    $('#slider1').tinycarousel({
      infinite: true,
      animation: true,
      animationTime: 500
    });

    $(function() {

      if ( $('#slider1 ul li').length == 3) {
        $('#slider1 .buttons').hide();
      }

    })

    $('.buildings .all_buildings .one_building .project_name').click(function() {
      
      if ($(this).parent().hasClass('active')) {
        $(this).parent().removeClass('active');
      } else {
        $('.buildings .all_buildings .one_building .project_name').parent().removeClass('active');
        $(this).parent().addClass('active');
      }
    });

    $('.fancybox').fancybox();

    // $('.modal1').fancybox();


 $(window).load(function() {
    $('.buildings .flexslider').flexslider({
      animation: "slide",
      directionNav: false,
      slideshow: false
    });

     $('.bot_slider .flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      animationSpeed: 1500
    });

    $('.modal .flexslider').flexslider({
      animation: "slide",
      controlNav: "thumbnails",
      directionNav: false
    });
  });

    $('.all_projects .one_project a,.wrapper_bot_slider .bot_slider li a').click(function() {
        var this_href = $(this).attr('href');
        $('.modal_all_bg').css({visibility:  'visible'});
        $(this_href).css({visibility:  'visible'});
        $('.modal').hide();
        $('.modal_all_bg').hide();
        $(this_href).fadeIn(500);
        $('.modal_all_bg').fadeIn(300);
        var Top_modal_window = $(window).height()/2-$(".modal").height()/2;
        var Left_modal_window = $(window).width()/2-$(".modal").width()/2;
        $(".modal").css({"top":Top_modal_window+"px","left":Left_modal_window+"px"});
        return false;
    });

    $(".modal_all_bg, .modal_close").click(function(event) {
        if (event.target.className == 'modal_all_bg' || event.target.className == 'modal_close') {
            $('.modal_all_bg,.modal').fadeOut(500);
        }
    });

});